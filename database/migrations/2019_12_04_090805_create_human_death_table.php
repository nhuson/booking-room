<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHumanDeathTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('human_death', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fullname')->nullable()->index();
            $table->string('slug')->nullable();
            $table->string('title')->nullable();
            $table->text('address')->nullable();
            $table->date('date_of_birth')->nullable()->index();
            $table->date('date_of_death')->nullable()->index();
            $table->integer('age')->nullable()->index();
            $table->tinyInteger('duration')->nullable();
            $table->integer('money_info')->nullable();
            $table->tinyInteger('service_charge')->nullable();
            $table->float('promotion', 8, 2)->default(0.00);
            $table->unsignedBigInteger('customers_id')->index();
            $table->foreign('customers_id')->references('id')->on('customers');
            $table->unsignedBigInteger('box_id')->index();
            $table->foreign('box_id')->references('id')->on('boxs');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('human_death');
    }
}
