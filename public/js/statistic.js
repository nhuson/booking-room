var StatisticFinance = (function() {

    function callAjax(buildingId) {
        $.ajax({
            type: "get",
            url: "/admin/ajax-statistic",
            data: {
                id: buildingId
            },
            // xhr: function() {
                
            // },
            beforeSend: function(xhr) {
                viewConfig(true);
            },
            complete: function() {
                viewConfig(false);
            }
        })
            .done(function(data) {
                $("#tableStatisticFinance")
                    .html("")
                    .html(data);
            })
            .fail(function(jXHR, textStatus) {
                console.log(jXHR);
            });
    }

    function changeSelect(elm) {
        $(elm).on("select2:select", function(e) {
            callAjax($(elm).val());
        });
    }

    function buildingStatistic(elm) {
        $(elm).select2({
            placeholder: "Toà nhà",
            width: "200"
        });
    }

    function viewConfig(show) {
        if (show) {
            $("#finance-loader").show();
            $("#tableStatisticFinance").css("opacity", 0.2);
        }else {
            $("#finance-loader").hide();
            $("#tableStatisticFinance").css("opacity", 1);
        }
       
    }

    function init() {
        viewConfig(false);
        buildingStatistic("#buildingStatistic");
        changeSelect("#buildingStatistic");
    }

    return {
        init: init
    };
})();
