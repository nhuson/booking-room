'use strict';

var HumanDeathForm = function () {
  var bodyPage = $('body');
  var durationMonthDefaul = 120;
  var durationFiftyYears = 600;
  var priceDurationDefaul = 18000000;
  var priceDurationFiftyYears = 39000000;
  var priceServiceDefaul = 200000;

  function calculatorPrice(durationSelector, serviceSelector, promotionSelector) {
    var durationMonth = $(durationSelector).val() == 0 ? durationMonthDefaul : durationFiftyYears ;
    var priceDuration = $(durationSelector).val() == 0 ? priceDurationDefaul : priceDurationFiftyYears ;
    var priceService = $(serviceSelector).val() == 0 ? priceServiceDefaul * durationMonth : 0 ;
    var pricePromo = $(promotionSelector).val() ? parseFloat($(promotionSelector).val()) : 0;
    var totalPrice = parseFloat(priceDuration) + parseFloat(priceService) - ( parseFloat(priceDuration) * pricePromo) / 100;
    $('#humanDeathMoneyInfo').val(totalPrice);
  }

  function changePriceRoom(durationSelector, serviceSelector, promotionSelector) {
    $(durationSelector).on("select2:select", function(e) { 
      calculatorPrice(durationSelector, serviceSelector, promotionSelector)
    });

    $(serviceSelector).on("select2:select", function(e) { 
      calculatorPrice(durationSelector, serviceSelector, promotionSelector)
    });

    $(promotionSelector).on("change", function(e) { 
      calculatorPrice(durationSelector, serviceSelector, promotionSelector)
    });
  }

  function init() {
    changePriceRoom('#humanDeathDuration', '#humanDeathServiceCharge', '#humanDeathPromotion');
  }

  return {
    init: init
  }
}();
