<div class="row">
	<a href="/admin/boxs">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-aqua">
				<i class="fa fa-inbox"></i>
			</span>
			<div class="info-box-content">
				<span class="info-box-text">Tổng số ô</span>
				<span class="info-box-number">{{ number_format($data['count-box']) }}</span>
			</div>
		</div>
	</div>
	</a>
	<a href="/admin/rooms">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-red">
				<i class="fa fa-cubes"></i>
			</span>
			<div class="info-box-content">
				<span class="info-box-text">Tổng số phòng</span>
				<span class="info-box-number">{{ number_format($data['count-room']) }}</span>
			</div>
		</div>
	</div>
	</a>
	<div class="clearfix visible-sm-block"></div>
	<a href="/admin/buildings">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-green">
				<i class="fa fa-building"></i>
			</span>
			<div class="info-box-content">
				<span class="info-box-text">Tổng số toà nhà</span>
				<span class="info-box-number">{{ number_format($data['count-building']) }}</span>
			</div>
		</div>
	</div>
	</a>
	<a href="/admin/customers">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-yellow">
				<i class="fa fa-users"></i>
			</span>
			<div class="info-box-content">
				<span class="info-box-text">Tổng số người liên hệ</span>
				<span class="info-box-number">{{ number_format($data['count-customer']) }}</span>
			</div>
		</div>
	</div>
	</a>
	<div class="clearfix visible-sm-block"></div>
	<a href="/admin/boxs?&id=&name=&box_type_id=&room_id=&status=0">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-blue">
				<i class="fa fa-folder-open-o"></i>
			</span>
			<div class="info-box-content">
				<span class="info-box-text">Tổng số ô còn trống</span>
				<span class="info-box-number">{{ number_format($data['count-box-empty']) }}</span>
			</div>
		</div>
	</div>
	</a>
	<a href="/admin/boxs?&id=&name=&box_type_id=&room_id=&status=1">
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-brone">
					<i class="fa fa-flag"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Tổng số ô đã đặt</span>
					<span class="info-box-number">{{ number_format($data['count-box-soldout']) }}</span>
				</div>
			</div>
		</div>
	</a>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-fuchsia">
					<i class="fa fa-diamond"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Tổng số tiền</span>
					<span class="info-box-number">{{ number_format($data['total-money']) }} VNĐ</span>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-olive">
					<i class="fa fa-money"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Tổng số tiền trong tháng</span>
					<span class="info-box-number">{{ number_format($data['total-money-month']) }} VNĐ</span>
				</div>
			</div>
		</div>
</div>

@include('admin.dashboard.chart', ["data" => $data])