<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<h4>Thống kê tài chính trong năm</h4>
		<canvas id="myChart" width="200" height="200"></canvas>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<h4>Thống kê tài chính trong tháng</h4>
		<canvas id="myChart2" width="200" height="200"></canvas>
	</div>
	<script>
	$(function () {
			var ctx = document.getElementById("myChart").getContext('2d');
			var ctx2 = document.getElementById("myChart2").getContext('2d');
			var myChart = new Chart(ctx, {
					type: 'line',
					data: {
							labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
							datasets: [{
									label: '# triệu VNĐ',
									data: [{{$data['data-money-year']}}],
									borderWidth: 1,
									backgroundColor: 'rgba(54, 162, 235, 0.2)'
							}]
					},
					options: {
							scales: {
									yAxes: [{
											ticks: {
													beginAtZero:true
											}
									}]
							}
					}
			});
			new Chart(ctx2, {
					type: 'bar',
					data: {
							labels: ["1", "2", "3", "4", "5", "6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"],
							datasets: [{
									label: '# triệu VNĐ',
									data: [{{$data['data-money-current-month']}}],
									borderWidth: 1,
									backgroundColor: 'rgba(255, 159, 64, 0.2)'
							}]
					},
					options: {
							scales: {
									yAxes: [{
											ticks: {
													beginAtZero:true
											}
									}]
							}
					}
			});
	});
	</script>
</div>