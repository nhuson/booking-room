<div id="tableStatisticFinance" >
  <table class="table table-responsive">
    <tbody>
      <tr>
        <td class="col">Tổng số ô: {{$data['building']->total_box}}</td>
        <td class="col">Tổng số ô đã đăng ký: {{$data['building']->total_box_soldout}}</td>
      </tr>
      <tr>
        <td class="col">Tổng số ô đơn: {{$data['building']->total_box_single}}</td>
        <td class="col">Tổng số ô đơn đã đăng ký: {{$data['building']->total_box_single_soldout}}</td>
      </tr>
      <tr>
        <td class="col">Tổng số ô đôi: {{$data['building']->total_box_dubble}}</td>
        <td class="col">Tổng số ô đôi đã đăng ký: {{$data['building']->total_box_dubble_soldout}}</td>
      </tr>
      <tr>
        <td colspan="2">Tổng số tiền của toà nhà: {{number_format($data['building']->total_price_building)}} VNĐ</td>
      </tr>
    </tbody>
  </table>
  <table class="table table-bordered vertical-align text-center table-responsive">
    <thead>
      <tr>
        <th rowspan="2">Phòng</th>
        <th colspan="4">Ô đơn</th>
        <th colspan="4">Ô đôi</th>
      </tr>
      <tr>
        <th>Tổng số ô</th>
        <th>Đã đăng ký</th>
        <th>Còn trống</th>
        <th>Tổng tiền (VNĐ)</th>
        <th>Tổng số ô</th>
        <th>Đã đăng ký</th>
        <th>Còn trống</th>
        <th>Tổng tiền (VNĐ)</th>
      </tr>
    </thead>
    @if(count($data['building']->rooms) > 0)
      <tbody>
        @foreach($data['building']->rooms as $room)
          <tr>
            <td>{{$room->name}}</td>
            <td>{{$room->count_box_single}}</td>
            <td>{{$room->count_box_single_soldout}}</td>
            <td>{{(int)$room->count_box_single - (int)$room->count_box_single_soldout}}</td>
            <td>{{number_format($room->count_money_single)}}</td>
            <td>{{$room->count_box_dubble}}</td>
            <td>{{$room->count_box_dubble_soldout}}</td>
            <td>{{(int)$room->count_box_dubble - (int)$room->count_box_dubble_soldout}}</td>
            <td>{{number_format($room->count_money_dubble)}}</td>
          </tr>
        @endforeach
      </tbody>
      <tfoot>
        <tr>
          <td>Tổng tiền (VNĐ)</td>
          <td colspan="4">{{number_format($data['building']->total_price_single)}}</td>
          <td colspan="4">{{number_format($data['building']->total_price_dubble)}}</td>
        </tr>
      </tfoot>
      @else
      <tbody>
        <tr><td colspan="9">Không có dữ liệu hiển thị.</td></tr>
      </tbody>  
    @endif
    
  </table>
</div>
