<style type="text/css">
  table.table-bordered{
    border:1px solid #001f3f!important;
    margin-top:20px;
  }
table.table-bordered > thead > tr > th{
    border:1px solid #001f3f!important;
}
table.table-bordered > tbody > tr > td{
    border:1px solid #001f3f!important;
}
table.table-bordered > tfoot > tr > td{
    border:1px solid #001f3f!important;
}

#finance-loader {
  display: none;
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 20px;
  height: 20px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
  position: absolute;
  top: 50%;
  left: 50%;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
#statisticFinance {
  position: relative;
}

</style>
<div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <label>Bảng theo dõi toà nhà</label>
      <select id="buildingStatistic" name="state">
        @foreach($data['buildings'] as $key => $building)
          @if($key == $data['building']->id)
            <option value="{{$key}}" selected>{{$building}}</option>
          @else
            <option value="{{$key}}">{{$building}}</option>
          @endif
        @endforeach
      </select>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div id="statisticFinance">
        <div id="finance-loader"></div>
        @include('admin.statistic.partion', ["data" => $data])
      </div>
    </div>
  </div>
</div>

@include('admin.dashboard.chart', ["data" => $data])