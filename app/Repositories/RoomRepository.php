<?php 
namespace App\Repositories;

use App\Interfaces\RoomInterface;
use App\Models\Rooms;

class RoomRepository implements RoomInterface {
  protected $model;

  public function __construct(Rooms $room) {
    $this->model = $room;
  }

  public function getNameAndId() {
    return $this->model->from('rooms')->pluck('name', 'id')->toArray();
  }

  public function getCountRoom() {
    return $this->model->from('rooms')->count();
  }

  public function getCountRoomByBuildingId($buildingId) {
    return $this->model->from('rooms')->where('building_id', $buildingId)->count();
  }

  public function getRoomById($id) {
    return $this->model->from('rooms')->where('id', $id)->first();
  }
}