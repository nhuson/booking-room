<?php
namespace App\Repositories;

use App\Interfaces\HumanDeathInterface;
use App\Models\HumanDeath;
use Carbon\Carbon;
use DB;

class HumanDeathRepository implements HumanDeathInterface {
  protected $model;

  public function __construct(HumanDeath $humanDeath) {
    $this->model = $humanDeath;
  }

  public function getHumanDeathById($id) {
    return $this->model->from('human_death')->where('id', $id)->first();
  }

  public function getHumanDeathToNotify() {
    $nowDay = Carbon::today()->format('d');
    $nowMonth = Carbon::today()->format('m');
    $fromDay = Carbon::today()->subDays(1)->format('d');
    return $this->model->from('human_death')
            ->select('fullname', 'title', 'age', 'address', 'date_of_birth', 'date_of_death', 'box_id', 'customers_id')
            ->whereMonth('date_of_death', $nowMonth)
            ->whereDay('date_of_death', '>=', $fromDay)
            ->whereDay('date_of_death', '<=', $nowDay)
            ->get();
  }

  public function getCountMoney($startDate, $endDate) {
    return $this->model->from('human_death')->select('money_info')->whereBetween('created_at', [$startDate, $endDate])->sum('money_info');
  }

  public function getTotalMoney() {
    return $this->model->from('human_death')->select('money_info')->sum('money_info');
  }

  public function getMoneyByYear() {
    $nowYear = Carbon::today()->format('Y');
    $arg = "(SELECT SUM(`money_info`) FROM human_death WHERE YEAR(`created_at`) = $nowYear AND month(`created_at`) ";
    return DB::table('human_death')->select(
      DB::raw("$arg = 1) as money_month_1"),
      DB::raw("$arg = 2) as money_month_2"),
      DB::raw("$arg = 3) as money_month_3"),
      DB::raw("$arg = 4) as money_month_4"),
      DB::raw("$arg = 5) as money_month_5"),
      DB::raw("$arg = 6) as money_month_6"),
      DB::raw("$arg = 7) as money_month_7"),
      DB::raw("$arg = 8) as money_month_8"),
      DB::raw("$arg = 9) as money_month_9"),
      DB::raw("$arg = 10) as money_month_10"),
      DB::raw("$arg = 11) as money_month_11"),
      DB::raw("$arg = 12) as money_month_12")
    )->first();
  }

  public function getMoneyByCurrentMonth() {
    $nowYear = Carbon::today()->format('Y');
    $nowMonth = Carbon::today()->format('m');
    $arg = "(SELECT SUM(`money_info`) FROM human_death WHERE YEAR(`created_at`) = $nowYear AND month(`created_at`) = $nowMonth AND DAY(`created_at`)";
    return DB::table('human_death')->select(
      DB::raw("$arg = 1) as day_1"),
      DB::raw("$arg = 2) as day_2"),
      DB::raw("$arg = 3) as day_3"),
      DB::raw("$arg = 4) as day_4"),
      DB::raw("$arg = 5) as day_5"),
      DB::raw("$arg = 6) as day_6"),
      DB::raw("$arg = 7) as day_7"),
      DB::raw("$arg = 8) as day_8"),
      DB::raw("$arg = 9) as day_9"),
      DB::raw("$arg = 10) as day_10"),
      DB::raw("$arg = 11) as day_11"),
      DB::raw("$arg = 12) as day_12"),
      DB::raw("$arg = 13) as day_13"),
      DB::raw("$arg = 14) as day_14"),
      DB::raw("$arg = 15) as day_15"),
      DB::raw("$arg = 16) as day_16"),
      DB::raw("$arg = 17) as day_17"),
      DB::raw("$arg = 18) as day_18"),
      DB::raw("$arg = 19) as day_19"),
      DB::raw("$arg = 20) as day_20"),
      DB::raw("$arg = 21) as day_21"),
      DB::raw("$arg = 22) as day_22"),
      DB::raw("$arg = 23) as day_23"),
      DB::raw("$arg = 24) as day_24"),
      DB::raw("$arg = 25) as day_25"),
      DB::raw("$arg = 26) as day_26"),
      DB::raw("$arg = 27) as day_27"),
      DB::raw("$arg = 28) as day_28"),
      DB::raw("$arg = 29) as day_29"),
      DB::raw("$arg = 30) as day_30"),
      DB::raw("$arg = 31) as day_31")
    )->first();
  }
}