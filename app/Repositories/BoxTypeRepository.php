<?php
namespace App\Repositories;

use App\Interfaces\BoxTypeInterface;
use App\Models\BoxType;

class BoxTypeRepository implements BoxTypeInterface {
  protected $model;

  public function __construct(BoxType $boxtype) {
    $this->model = $boxtype;
  }

  public function getBoxType() {
    return $this->model->from('box_type')->pluck('name', 'id')->toArray();
  }
}