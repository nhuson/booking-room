<?php
namespace App\Repositories;

use App\Interfaces\BoxInterface;
use App\Models\Boxs;
use DB;

class BoxRepository implements BoxInterface {
  protected $model;

  public function __construct(Boxs $box) {
    $this->model = $box;
  }

  public function getNameAndId() {
    return $this->model->from('boxs')->pluck('name', 'id')->toArray();
  }

  public function getNameAndIdWithStatus($boxId) {
    if ($boxId) {
      return $this->model->from('boxs')->where('status', 0)->orWhere('id', $boxId)->pluck('name', 'id')->toArray();
    }
    return $this->model->from('boxs')->where('status', 0)->pluck('name', 'id')->toArray();

  }

  public function getCountBox() {
    return $this->model->from('boxs')->count();
  }

  public function getCountBoxEmpty() {
    return $this->model->from('boxs')->where('status', 0)->count();
  }

  public function getCountBoxSoldout() {
    return $this->model->from('boxs')->where('status', 1)->count();
  }

  public function getCountBoxByRoomId($roomId) {
    return $this->model->from('boxs')->where('room_id', $roomId)->count();
  }

  public function getBoxById($id) {
    return $this->model->from('boxs')->where('id', $id)->first();
  }

  public function findOneAndUpdate($id, $data) {
    return $this->model->from('boxs')->where('id', $id)->update($data);
  }

  public function getBoxCountSingle() {
    return $this->model->select(DB::raw("COUNT(*) as count_box"), "box_type_id")->from('boxs')->groupBy("box_type_id")->pluck('count_box', 'box_type_id')->toArray();
  }

  public function getBoxStatusCountSingle() {
    return DB::table('boxs')->select(
      DB::raw("(SELECT COUNT(*) FROM boxs WHERE box_type_id = 1 AND status = 1) as box_single_status ") ,
      DB::raw("(SELECT COUNT(*) FROM boxs WHERE box_type_id = 2 AND status = 1) as box_dubble_status")
    )->first();
  }

}