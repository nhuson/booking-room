<?php
namespace App\Repositories;

use App\Interfaces\BuildingInterface;
use App\Models\Building;

class BuildingRepository implements BuildingInterface {
  protected $model;

  public function __construct(Building $building) {
    $this->model = $building;
  }

  public function getCountBuilding() {
    return $this->model->from('building')->count();
  }

  public function getNameAndId() {
    return $this->model->from('building')->pluck('name', 'id')->toArray();
  }

  public function getNameById($id) {
    return $this->model->select('name')->from('building')->where('id', $id)->first();
  }

  public function getRoomNumber($id) {
    return $this->model->select('number_rooms')->from('building')->where('id', $id)->first();
  }

  public function getBuildingFirst() {
    return $this->model->from('building')->first();
  }

  public function getBuildingById($id) {
    return $this->model->from('building')->where('id', $id)->first();
  }
}