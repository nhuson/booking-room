<?php 
namespace App\Repositories;

use App\Interfaces\CustomerInterface;
use App\Models\Customers;

class CustomerRepository implements CustomerInterface {
  protected $model;

  public function __construct(Customers $customer) {
    $this->model = $customer;
  }

  public function getNameAndId() {
    return $this->model->from('customers')->pluck('name', 'id')->toArray();
  }

  public function getCountCustomer() {
    return $this->model->from('customers')->count();
  }
}