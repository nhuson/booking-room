<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\HumanDeathService;
use App\Notifications\BookRoomNotification;
use Carbon\Carbon;

class SendSmsToCustommer extends Command
{
    protected $humanDeathService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:sms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send SMS notifi custommer for near date of death every year.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(HumanDeathService $humanDeathService)
    {
        parent::__construct();
        $this->humanDeathService = $humanDeathService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $humansNotify = $this->humanDeathService->sendSmsNotifyToCustomer();
        foreach($humansNotify as $human) {
            $deathDate = Carbon::parse($human->date_of_death)->format('d-m');
            $message = "Sắp đến ngày $deathDate là ngày giỗ của $human->title $human->fullname.";
            $human->customers->notify(new BookRoomNotification($message));
        }
    }
}
