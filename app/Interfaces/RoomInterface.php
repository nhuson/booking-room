<?php 

namespace App\Interfaces;
use App\Interfaces\BaseInterface;

interface RoomInterface extends BaseInterface {
  public function getNameAndId();
  public function getCountRoom();
  public function getCountRoomByBuildingId($buildingId);
  public function getRoomById($id);
}