<?php
namespace App\Interfaces;
use App\Interfaces\BaseInterface;

interface CustomerInterface extends BaseInterface {
  public function getNameAndId();
  public function getCountCustomer();
}