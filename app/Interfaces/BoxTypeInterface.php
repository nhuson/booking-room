<?php
namespace App\Interfaces;
use App\Interfaces\BaseInterface;

interface BoxTypeInterface extends BaseInterface {
  public function getBoxType();
}