<?php 

namespace App\Interfaces;
use App\Interfaces\BaseInterface;

interface HumanDeathInterface extends BaseInterface {
  public function getHumanDeathById($id);
  public function getHumanDeathToNotify();
  public function getCountMoney($startDate, $endDate);
  public function getTotalMoney();
}