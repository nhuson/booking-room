<?php 

namespace App\Interfaces;
use App\Interfaces\BaseInterface;

interface BoxInterface extends BaseInterface {
  public function getNameAndId();
  public function getCountBox();
  public function getCountBoxEmpty();
  public function getCountBoxSoldout();
  public function getCountBoxByRoomId($roomId);
  public function getBoxById($id);
  public function findOneAndUpdate($id, $data);
  public function getNameAndIdWithStatus($boxId);
}