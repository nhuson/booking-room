<?php
namespace App\Interfaces;
use App\Interfaces\BaseInterface;

interface BuildingInterface extends BaseInterface {
  public function getCountBuilding();
  public function getNameAndId();
  public function getNameById($id);
  public function getRoomNumber($id);
}