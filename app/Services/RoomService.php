<?php
namespace App\Services;
use App\Interfaces\RoomInterface;
use Illuminate\Support\MessageBag;

class RoomService {
  protected $roomRepo;

  public function __construct(RoomInterface $roomRepo) {
    $this->roomRepo = $roomRepo;
  }

  public function getNameAndId() {
    return $this->roomRepo->getNameAndId();
  }

  public function getCountRoom() {
    return $this->roomRepo->getCountRoom();
  }

  public function getCountRoomByBuildingId($buildingId) {
    return $this->roomRepo->getCountRoomByBuildingId($buildingId);
  }

  public function getRoomById($id) {
    return $this->roomRepo->getRoomById($id);
  }

  public function handlerForm($form, $buildingService, $roomService) {
    $roomCountMax = $buildingService->getRoomNumber((int)$form->building_id);
    $roomCount = $roomService->getCountRoomByBuildingId((int)$form->building_id);
    $error = new MessageBag([
      'title'   => 'Cảnh báo.',
      'message' => 'Số lượng phòng của toà nhà đã đến giới hạn.',
    ]);
    return $roomCount >= $roomCountMax ? $error : false;
    
  }
}