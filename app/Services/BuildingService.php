<?php
namespace App\Services;
use App\Interfaces\BuildingInterface;
use App\Interfaces\RoomInterface;

class BuildingService {
  protected $buildingRepo, $roomRepo;

  public function __construct(BuildingInterface $buildingRepo, RoomInterface $roomRepo) {
    $this->buildingRepo = $buildingRepo;
    $this->roomRepo = $roomRepo;
  }

  public function getCountBuilding() {
    return $this->buildingRepo->getCountBuilding();
  }

  public function getNameAndId() {
    return $this->buildingRepo->getNameAndId();
  }

  public function getNameById($id) {
    $building = $this->buildingRepo->getNameById($id);
    return $building->name;
  }

  public function getRoomNumber($id) {
    $building = $this->buildingRepo->getRoomNumber($id);
    return $building->number_rooms;
  }

  public function checkDelete($id) {
    $countRoom = $this->roomRepo->getCountRoomByBuildingId($id);
    if ($countRoom > 0) {
      return false;
    }

    return true;
  }

  public function getBuildingFirst() {
    return $this->buildingRepo->getBuildingFirst();
  }

  public function getBuildingById($id) {
    return $this->buildingRepo->getBuildingById($id);
  }
}