<?php
namespace App\Services;

use Illuminate\Support\Str;
use Encore\Admin\Grid\Exporters\ExcelExporter;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Excel;
use App\Interfaces\HumanDeathInterface;
define('FILENAME', "Thống kê tài chính " . date('d-m-Y') . ".xlsx");

class CollectionExport implements FromCollection, WithHeadings
{
    use Exportable;
    protected $data, $humanRepo;

    public function __construct($collections, $humanRepo) {
      $this->data = $collections;
      $this->humanRepo = $humanRepo;
    }

    public function collection()
    {
      $dataExport = [];
      foreach($this->data as $item) {
        $human = $this->humanRepo->getHumanDeathById($item['id']);
        
        array_push($dataExport, [
          'id' => $human->id,
          'building' => $human->boxs->rooms->building->name ?? "",
          'room' => $human->boxs->rooms->name ?? "",
          'box' => $human->boxs->name ?? "",
          'fullname' => $human->fullname ?? "",
          'title' => $human->title ?? "",
          'money_info' => $human->money_info ?? "",
        ]);
      }
      return collect($dataExport);
    }

    public function headings(): array
    {
        return [
            'ID',
            'Toà nhà',
            'Tên phòng',
            'Tên ô',
            'Họ tên',
            'Chức danh',
            'Thành tiền'
        ];
    }

}

class FinanceExcelService extends ExcelExporter {
  protected $humanRepo;

  public function __construct($humanRepo) {
    $this->humanRepo = $humanRepo;
  }

  public function export() {
    Excel::download(new CollectionExport($this->getData(), $this->humanRepo), FILENAME)->prepare(request())->send();
    exit;
  }
}