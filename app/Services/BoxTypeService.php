<?php
namespace App\Services;
use App\Interfaces\BoxTypeInterface;

class BoxTypeService {
  protected $boxtypeRepo;

  public function __construct(BoxTypeInterface $boxtypeRepo) {
    $this->boxtypeRepo = $boxtypeRepo;
  }

  public function getBoxtype() {
    return $this->boxtypeRepo->getBoxType();
  }
}