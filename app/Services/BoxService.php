<?php
namespace App\Services;
use App\Interfaces\BoxInterface;
use Illuminate\Support\MessageBag;

class BoxService {
  protected $boxRepo;

  public function __construct(BoxInterface $boxRepo) {
    $this->boxRepo = $boxRepo;
  }

  public function getNameAndId() {
    return $this->boxRepo->getNameAndId();
  }

  public function getNameAndIdWithStatus($boxId) {
    return $this->boxRepo->getNameAndIdWithStatus($boxId);
  }

  public function getCountBox() {
    return $this->boxRepo->getCountBox();
  }

  public function getCountBoxEmpty() {
    return $this->boxRepo->getCountBoxEmpty();
  }

  public function getCountBoxSoldout() {
    return $this->boxRepo->getCountBoxSoldout();
  }

  public function getCountBoxByRoomId($roomId) {
    return $this->boxRepo->getCountBoxByRoomId($roomId);
  }

  public function getBoxById($id) {
    return $this->boxRepo->getBoxById($id);
  }

  public function handlerForm($form, $boxService, $roomService) {
    $room = $roomService->getRoomById((int)$form->room_id);
    $boxCountMax = $room->number_box;
    $boxCount = $boxService->getCountBoxByRoomId((int)$form->room_id);
    $error = new MessageBag([
      'title'   => 'Cảnh báo.',
      'message' => 'Số lượng ô của phòng đã đến giới hạn.',
    ]);
    return $boxCount >= $boxCountMax ? $error : false;
  }

  public function findOneAndUpdate($id, $data) {
    return $this->boxRepo->findOneAndUpdate($id, $data);
  }

  public function getBoxCountSingle() {
    return $this->boxRepo->getBoxCountSingle();
  }

  public function getBoxStatusCountSingle() {
    return $this->boxRepo->getBoxStatusCountSingle();
  }
}