<?php
namespace App\Services;
use App\Interfaces\CustomerInterface;

class CustomerService {
  protected $customerRepo;

  public function __construct(CustomerInterface $customerRepo) {
    $this->customerRepo = $customerRepo;
  }

  public function getNameAndId() {
    return $this->customerRepo->getNameAndId();
  }

  public function getCountCustomer() {
    return $this->customerRepo->getCountCustomer();
  }
}