<?php
namespace App\Services;
use App\Interfaces\HumanDeathInterface;
use App\Services\CustomerService;

class HumanDeathService {
  protected $humanDeathRepo;

  public function __construct(HumanDeathInterface $humanDeathRepo) {
    $this->humanDeathRepo = $humanDeathRepo;
  }

  public function getHumanDeathById($id) {
    return $this->humanDeathRepo->getHumanDeathById($id);
  }

  public function sendSmsNotifyToCustomer() {
    return $this->humanDeathRepo->getHumanDeathToNotify();
  }

  public function getCountMoney($startDate, $endDate) {
    return $this->humanDeathRepo->getCountMoney($startDate, $endDate);
  }

  public function getTotalMoney() {
    return $this->humanDeathRepo->getTotalMoney();
  }

  public function getMoneyByYear() {
    $result = [];
    $data = $this->humanDeathRepo->getMoneyByYear();
    if ($data) {
      foreach($data as $value) {
        array_push($result, (float)$value / 1000000);
      }
    }

    return implode(",", $result);
  }

  public function getMoneyByCurrentMonth() {
    $result = [];
    $data = $this->humanDeathRepo->getMoneyByCurrentMonth();
    if ($data) {
      foreach($data as $value) {
        array_push($result, (float)$value / 1000000);
      }
    }
    
    return implode(",", $result);
  }
}