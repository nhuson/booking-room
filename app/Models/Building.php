<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use common\Helper;

class Building extends Model
{
    protected $table = 'building';
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->slug = Helper::convert_string($model->name);
        });
    }

    public function rooms() {
        return $this->hasMany('App\Models\Rooms', 'building_id');
    }

    public function getTotalBoxAttribute() {
        $rooms = $this->rooms()->get();
        $total = 0;
        if (count($rooms) == 0) {
            return 0;
        }
        foreach($rooms as $room) {
            $total += $room->boxs()->count();
        }
        
        return $total;
    }

    public function getTotalBoxSoldoutAttribute() {
        $rooms = $this->rooms()->get();
        $total = 0;
        if (count($rooms) == 0) {
            return 0;
        }
        foreach($rooms as $room) {
            $total += $room->boxs()->where('status', 1)->count();
        }
        
        return $total;
    }

    public function getTotalBoxSingleAttribute() {
        $rooms = $this->rooms()->get();
        $total = 0;
        if (count($rooms) == 0) {
            return 0;
        }
        foreach($rooms as $room) {
            $total += $room->count_box_single;
        }
        
        return $total;
    }

    public function getTotalBoxSingleSoldoutAttribute() {
        $rooms = $this->rooms()->get();
        $total = 0;
        if (count($rooms) == 0) {
            return 0;
        }
        foreach($rooms as $room) {
            $total += $room->count_box_single_soldout;
        }
        
        return $total;
    }

    public function getTotalBoxDubbleAttribute() {
        $rooms = $this->rooms()->get();
        $total = 0;
        if (count($rooms) == 0) {
            return 0;
        }
        foreach($rooms as $room) {
            $total += $room->count_box_dubble;
        }
        
        return $total;
    }

    public function getTotalBoxDubbleSoldoutAttribute() {
        $rooms = $this->rooms()->get();
        $total = 0;
        if (count($rooms) == 0) {
            return 0;
        }
        foreach($rooms as $room) {
            $total += $room->count_box_dubble_soldout;
        }
        
        return $total;
    }

    public function getTotalPriceSingleAttribute() {
        $rooms = $this->rooms()->get();
        $total = 0;
        if (count($rooms) == 0) {
            return 0;
        }
        foreach($rooms as $room) {
            
            $total += $room->count_money_single;
            
        }
        return $total;
    }

    public function getTotalPriceDubbleAttribute() {
        $rooms = $this->rooms()->get();
        $total = 0;
        if (count($rooms) == 0) {
            return 0;
        }
        foreach($rooms as $room) {
            $total += $room->count_money_dubble;
        }
        
        return $total;
    }

    public function getTotalPriceBuildingAttribute() {
        return $this->total_price_dubble + $this->total_price_single;
    }
}
