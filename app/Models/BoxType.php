<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use common\Helper;

class BoxType extends Model
{
    protected $table = 'box_type';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->slug = Helper::convert_string($model->name);
        });
    }

    public function boxs() {
        return $this->hasMany('App\Models\Boxs', 'box_type_id');
    }
}
