<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use common\Helper;

class Boxs extends Model
{
    protected $table = 'boxs';
    protected $fillable = ['customers_id'];
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->slug = Helper::convert_string($model->fullname);
        });
    }

    public function boxTypes() {
        return $this->belongsTo('App\Models\BoxType', 'box_type_id');
    }

    public function rooms() {
        return $this->belongsTo('App\Models\Rooms', 'room_id');
    }

    public function humanDeath() {
        return $this->hasOne('App\Models\HumanDeath', 'box_id');
    }
}
