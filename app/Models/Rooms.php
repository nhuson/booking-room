<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use common\Helper;

class Rooms extends Model
{
    protected $table = 'rooms';

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->slug = Helper::convert_string($model->name);
        });
    }


    public function building() {
        return $this->belongsTo('App\Models\Building', 'building_id');
    }

    public function boxs() {
        return $this->hasMany('App\Models\Boxs', 'room_id');
    }

    public function getCountBoxSingleAttribute() {
        return $this->boxs()->where("box_type_id", 1)->count();
    }

    public function getCountBoxSingleSoldoutAttribute() {
        return $this->boxs()->where("box_type_id", 1)->where("status", 1)->count();
    }

    public function getCountMoneySingleAttribute() {
        $boxs = $this->boxs()->where('box_type_id', 1)->get();
        $total = 0;
        if (count($boxs) == 0) {
            return 0;
        }
        foreach($boxs as $box) {
            $total += $box->humanDeath()->sum('money_info');
        }
        
        return $total;
    }

    public function getCountBoxDubbleAttribute() {
        return $this->boxs()->where("box_type_id", 2)->count();
    }

    public function getCountBoxDubbleSoldoutAttribute() {
        return $this->boxs()->where("box_type_id", 2)->where("status", 1)->count();
    }

    public function getCountMoneyDubbleAttribute() {
        $boxs = $this->boxs()->where('box_type_id', 2)->get();
        $total = 0;
        if (count($boxs) == 0) {
            return 0;
        }
        foreach($boxs as $box) {
            $total += $box->humanDeath()->sum('money_info');
        }
        
        return $total;
    }
}
