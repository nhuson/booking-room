<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Customers extends Model
{
    use Notifiable;

    protected $table = 'customers';

    public function boxs() {
        return $this->hasMany('App\Models\Boxs', 'customers_id');
    }

    // public function getPhoneAttribute($phone) {
    //     return preg_replace('/^0/','84', $phone);
    // }

    /**
     * Route notifications for the Nexmo channel.
     *
     * @return string
     */
    public function routeNotificationForNexmo()
    {
        return preg_replace('/^0/','84', $this->phone);
    }
}
