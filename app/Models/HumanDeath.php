<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class HumanDeath extends Model
{
    protected $table = 'human_death';

    public function customers() {
        return $this->belongsTo('App\Models\Customers', 'customers_id');
    }

    public function boxs() {
        return $this->belongsTo('App\Models\Boxs', 'box_id');
    }

    public function setDateOfBirthAttribute($value) {
        $this->attributes['date_of_birth'] =  Carbon::parse($value)->format('Y-m-d');
    }

    public function setDateOfDeathAttribute($value) {
        $this->attributes['date_of_death'] =  Carbon::parse($value)->format('Y-m-d');
    }
}
