<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {
    $router->get('/', 'HomeController@index')->name('admin.home');
    $router->resource('/customers', CustomerController::class);
    $router->resource('/box-type', BoxtypeController::class);
    $router->resource('/boxs', BoxController::class);
    $router->resource('/rooms', RoomController::class);
    $router->resource('/buildings', BuildingController::class);
    $router->resource('/human-death', HumanDeathController::class);
    $router->resource('/finance', FinanceController::class);
    $router->get('/statistic', 'FinanceController@statistic')->name('finance.statistic');
    $router->get('/ajax-statistic', 'FinanceController@ajaxStatistic')->name('finance.statistic.ajax');
});
