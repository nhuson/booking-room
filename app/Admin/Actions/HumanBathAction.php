<?php

namespace App\Admin\Actions;

use Encore\Admin\Actions\BatchAction;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\BoxRepository;
use App\Models\Boxs;

class HumanBathAction extends BatchAction
{
    public $name = 'Xoá mục đã chọn';

    public function handle(Collection $collection)
    {
        $boxService = new BoxRepository(new Boxs);
        $trans = [
            'failed'    => trans('admin.delete_failed'),
            'succeeded' => trans('admin.delete_succeeded'),
        ];

        foreach ($collection as $model) {
            try {
                $boxService->findOneAndUpdate($model->box_id, ["status" => 0]);
                $model->delete();
            } catch (\Exception $exception) {
                return $this->response()->error("Item chọn xoá bao gồm các mục phụ thuộc. Vui lòng xoá các mục phụ thuộc trước.");
            break;
            }
        }
       
        
        return $this->response()->success('Success message...')->refresh();
    }

    public function dialog()
    {
        $this->question(trans('admin.delete_confirm'), '', ['confirmButtonColor' => '#d33']);
    }

}