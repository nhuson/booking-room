<?php

namespace App\Admin\Actions;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\BoxRepository;
use App\Models\Boxs;

class HumanDeathAction extends RowAction
{

    public $name = 'Xoá';

    public function handle(Model $model)
    {
        $boxService = new BoxRepository(new Boxs);
        $trans = [
            'failed'    => trans('admin.delete_failed'),
            'succeeded' => trans('admin.delete_succeeded'),
        ];
        try {
            $boxService->findOneAndUpdate($model->box_id, ["status" => 0]);
            $model->delete();
        } catch (\Exception $exception) {
            return $this->response()->error("Item chọn xoá bao gồm các mục phụ thuộc. Vui lòng xoá các mục phụ thuộc trước.");
        }

        return $this->response()->success($trans['succeeded'])->refresh();
    }

    public function dialog()
    {
        $this->question(trans('admin.delete_confirm'), '', ['confirmButtonColor' => '#d33']);
    }

}