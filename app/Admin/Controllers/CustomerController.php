<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Customers;
use Illuminate\Support\MessageBag;

class CustomerController extends AdminController
{
    public function __construct() {
    }
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Quản lý người liên hệ';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Customers);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('name', __('Họ và tên'))->editable();
        $grid->column('address', __('Địa chỉ'))->limit(30)->editable('textarea');
        $grid->column('phone', __('Số điện thoại'))->editable();
        $grid->column('email', __('Email'))->editable();
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->filter(function($filter) {
            $filter->like('name', __('Họ và tên'));
            $filter->like('email', __('Email'));
            $filter->like('phone', __('Số điện thoại'));
            $filter->like('address', __('Địa chỉ'));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Customers::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('name', __('Họ và tên'));
        $show->field('phone', __('Số điện thoại'));
        $show->field('email', __('Email'));
        $show->field('address', __('Địa chỉ'));
        $show->field('facebook', __('Facebook'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->panel()
        ->tools(function ($tools) {
            $tools->disableDelete();
        });
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Customers);
        $form->display('id', __('ID'));
        $form->text('name', __('Họ và tên'))->placeholder('Họ và tên...');
        $form->mobile('phone', __('Số điện thoại'))->placeholder('Số điện thoại...');
        $form->email('email', __('Email'))->placeholder('Email...');
        $form->text('facebook', __('Facebook'))->placeholder('Facebook...')->icon('fa-facebook-official');
        $form->textarea('address', __('Địa chỉ'))->placeholder('Địa chỉ...')->rows(10);
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });
        return $form;
    }
}
