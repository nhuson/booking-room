<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use App\Services\BuildingService;
use App\Services\BoxService;
use App\Services\RoomService;
use App\Services\CustomerService;
use App\Services\HumanDeathService;
use Carbon\Carbon;

class HomeController extends Controller
{
    protected $buildingService, $boxService, $roomService, $customerService, $humanService;

    public function __construct(BuildingService $buildingService, BoxService $boxService, RoomService $roomService, CustomerService $customerService, HumanDeathService $humanService) {
        $this->buildingService = $buildingService;
        $this->boxService = $boxService;
        $this->roomService = $roomService;
        $this->customerService = $customerService;
        $this->humanService = $humanService;
    }

    public function index(Content $content)
    {
        $startDate = Carbon::now()->startOfMonth()->toDateString();
        $endDate = Carbon::now()->endOfMonth()->toDateString();
        $data = [
            "count-box" => $this->boxService->getCountBox(),
            "count-box-empty" => $this->boxService->getCountBoxEmpty(),
            "count-box-soldout" => $this->boxService->getCountBoxSoldout(),
            "count-building" => $this->buildingService->getCountBuilding(),
            "count-room" => $this->roomService->getCountRoom(),
            "count-customer" => $this->customerService->getCountCustomer(),
            "total-money" => $this->humanService->getTotalMoney(),
            "total-money-month" => $this->humanService->getCountMoney($startDate, $endDate),
            "data-money-year" => $this->humanService->getMoneyByYear(),
            "data-money-current-month" => $this->humanService->getMoneyByCurrentMonth()
        ];
        return $content
            ->title('Dashboard')
            ->description('Description...')
            ->body(view('admin.dashboard.index', ["data" => $data]));
    }
}
