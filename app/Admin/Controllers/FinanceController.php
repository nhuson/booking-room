<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Layout\Content;
use App\Models\HumanDeath;
use App\Services\HumanDeathService;
use App\Services\FinanceExcelService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use App\Interfaces\HumanDeathInterface;
use App\Services\BuildingService;
use App\Services\BoxService;

class FinanceController extends AdminController
{
    protected $humanService, $humanRepo, $buildingService, $boxService;

    public function __construct(HumanDeathService $humanService, HumanDeathInterface $humanRepo, BuildingService $buildingService, BoxService $boxService) {
        $this->humanService = $humanService;
        $this->humanRepo = $humanRepo;
        $this->buildingService = $buildingService;
        $this->boxService = $boxService;
    }
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Quản lý tài chính';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new HumanDeath);
        $humanRepo = $this->humanRepo;
        $requests = Request::query();
        $grid->disableCreateButton();
        $grid->disableRowSelector();
        $grid->disableActions();
        $startDate = isset($requests['created_at']['start']) ? $requests['created_at']['start'] : Carbon::now()->startOfMonth()->toDateString();
        $endDate = isset($requests['created_at']['end']) ? $requests['created_at']['end'] :Carbon::now()->endOfMonth()->toDateString();
        $totalMoney = $this->humanService->getCountMoney($startDate, $endDate);
        $grid->header(function ($query) use($totalMoney) {
            return "Tổng số tiền thanh toán: <strong>" . number_format($totalMoney) . "</strong> VNĐ";
        });
        $grid->column('id', __('ID'))->sortable()->filter();
        $grid->column(__('Tòa nhà'))->display(function (){
          return $this->boxs->rooms->building->name;
        });
        $grid->column(__('Phòng'))->display(function (){
          return $this->boxs->rooms->name;
        });
        $grid->column('boxs.name', __('Tên Ô'));
        $grid->column('fullname', __('Họ và tên'))->sortable()->filter('like');
        $grid->duration(__('Thời hạn'))->display(function($duration) {
          return $duration == 0 ? "10 năm" : "50 năm";
        });
        $grid->money_info(__('Thành tiền'))->display(function($money) {
          return number_format($money) . " VNĐ";
        });
        $grid->created_at(__('Ngày đăng ký'))->display(function($date) {
          return Carbon::parse($date)->diffForHumans();
        });

        $grid->filter(function($filter) {
          $filter->disableIdFilter();
          $filter->between('created_at', __('Tìm kiếm theo ngày'))->datetime();
        });
        
        $grid->exporter(new FinanceExcelService($humanRepo));

        return $grid;
    }

    public function statistic(Content $content) {
      $data = [
        "buildings" => $this->buildingService->getNameAndId(),
        "building" => $this->buildingService->getBuildingFirst(),
        "data-money-year" => $this->humanService->getMoneyByYear(),
        "data-money-current-month" => $this->humanService->getMoneyByCurrentMonth()
      ];

      return $content
            ->title('Thống kê tài chính')
            ->breadcrumb(
              ['text' => 'Thống kê tài chính', 'url' => '/admin/statistic']
            )
            ->description('...')
            ->body(view('admin.statistic.index', ["data" => $data]));
    }

    public function ajaxStatistic(\Illuminate\Http\Request $request) {
      if (Request::ajax()) {
        $data = [
          "building" => $this->buildingService->getBuildingById($request->id),
          "data-money-year" => $this->humanService->getMoneyByYear(),
          "data-money-current-month" => $this->humanService->getMoneyByCurrentMonth()
        ];

        return response()->view('admin.statistic.partion', ["data" => $data]);
      }
    }
}
