<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Rooms;
use App\Services\BuildingService;
use App\Services\RoomService;
use Illuminate\Support\MessageBag;

class RoomController extends AdminController
{
    protected $buildingService, $roomService;

    public function __construct(BuildingService $buildingService, RoomService $roomService) {
        $this->buildingService = $buildingService;
        $this->roomService = $roomService;
    }
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Quản lý phòng';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Rooms);

        $grid->column('id', __('ID'))->sortable()->filter();
        $grid->column('name', __('Tên phòng'))->editable()->sortable()->filter('like');
        $grid->column('name_floor', __('Tên tầng'))->editable()->sortable()->filter('like');
        $grid->column('number_box', __('Số lượng ô'))->editable()->sortable()->filter();
        $grid->column('building.name', __('Toà nhà'));

        $grid->filter(function($filter) {
            $filter->like('name', __('Tên phòng'));
            $filter->like('name_floor', __('Tên tầng'));
            $filter->equal('building_id', 'Toà nhà')->select($this->buildingService->getNameAndId());
            $filter->equal('number_box', 'Số lượng ô');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Rooms::findOrFail($id));
        $show->field('id', __('ID'));
        $show->field('name', __('Tên phòng'));
        $show->field('name_floor', __('Tên tầng'));
        $show->field('number_box', __('Số lượng ô'));
        $show->building(__('Toà nhà'), function($building) {
            $building->setResource('/admin/buildings');
            $building->panel()
            ->tools(function ($tools) {
                $tools->disableDelete();
            });
            $building->name(__("Toà nhà"));
        });
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->panel()
        ->tools(function ($tools) {
            $tools->disableDelete();
        });
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Rooms);
        $buildingService = $this->buildingService;
        $roomService = $this->roomService;
        $form->display('id', __('ID'));
        $form->text('name', __('Tên phòng'))->placeholder('Tên phòng...')->rules('required');
        $form->text('name_floor', __('Tên tầng'))->placeholder('Tên tầng...');
        $form->number('number_box', __('Số lượng ô'))->placeholder('Số lượng ô...')->rules('required');
        $form->select('building_id', __("Toà nhà"))->options($this->buildingService->getNameAndId())->rules('required');

        $form->display('created_at', __('Created At'));
        $form->display('updated_at', __('Updated At'));

        $form->saving(function ($form) use ($buildingService, $roomService) {
            if (!$form->model()->id) {
                $error = $roomService->handlerForm($form, $buildingService, $roomService);
                if ($error) {
                    return back()->with(compact('error'));
                }
            } else {
                $oldRoom = $roomService->getRoomById($form->model()->id);
                if ($form->building_id) {
                    if ((int)$oldRoom->building_id != (int)$form->building_id) {
                        $error = $roomService->handlerForm($form, $buildingService, $roomService);
                        if ($error) {
                            return back()->with(compact('error'));
                        }
                    }
                }
            }
        });
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });

        return $form;
    }
}
