<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\HumanDeath;
use App\Services\BoxTypeService;
use App\Services\CustomerService;
use App\Services\BuildingService;
use App\Services\BoxService;
use App\Services\HumanDeathService;
use App\Admin\Actions\HumanDeathAction;
use App\Admin\Actions\HumanBathAction;
use Carbon\Carbon;

define("DURATION_MONTH_DEFAUTL", 120);
define("DURATION_FIFTY_YEARS", 600);
define("PRICE_DURATION_DEFAUTL", 18000000);
define("PRICE_DURATION_FIFTY_YEARS", 39000000);
define("PRICE_SERVICE_DEFAULT", 200000);

class HumanDeathController extends AdminController
{
    protected $boxtypeService, $customerService, $boxService, $buildingService, $humanService;

    public function __construct(BuildingService $buildingService, BoxTypeService $boxtypeService, CustomerService $customerService, BoxService $boxService, HumanDeathService $humanService) {
        $this->boxtypeService = $boxtypeService;
        $this->customerService = $customerService;
        $this->buildingService = $buildingService;
        $this->boxService = $boxService;
        $this->humanService = $humanService;
    }
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Quản lý ô đăng ký';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new HumanDeath);
        $buildingService = $this->buildingService;
        $grid->column('id', __('ID'))->sortable()->filter();
        $grid->column('fullname', __('Họ và tên'))->editable()->sortable()->filter('like');
        $grid->column('title', __('Chức danh'))->editable()->sortable()->filter('like');
        $grid->column('age', __('Tuổi'))->editable();
        $grid->column('boxs.name', __('Ô'));
        $grid->customers(__('Người liên hệ'))->display(function($customer) {
            $customerName = $customer['name'];
            return "<a href='/admin/customers/" . $customer['id'] . "'>$customerName</a>";
        });
        $grid->filter(function($filter) {
            $filter->like('fullname', __('Họ và tên'));
            $filter->like('title', 'Chức danh');
            $filter->equal('customers_id', 'Người liên hệ')->select($this->customerService->getNameAndId());
            $filter->equal('box_id', 'Ô')->select($this->boxService->getNameAndId());
            $filter->equal('age', __('Tuổi'))->integer();
            $filter->like('address', __('Địa chỉ'));
            $filter->where(function ($query) {
                $query->whereDate('date_of_birth', Carbon::parse($this->input));
            }, 'Ngày sinh')->datetime(['format' => 'DD-MM-YYYY']);
            $filter->where(function ($query) {
                $query->whereDate('date_of_death', Carbon::parse($this->input));
            }, 'Ngày mất')->datetime(['format' => 'DD-MM-YYYY']);
        });
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });
        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->add(new HumanDeathAction());
        });
        $grid->batchActions(function ($batch) {
            $batch->add(new HumanBathAction());
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(HumanDeath::findOrFail($id));
        $show->field('id', __('ID'));
        $show->field('fullname', __('Họ và tên'));
        $show->customers(__("Người liên hệ"), function($customer){
            $customer->setResource('/admin/customers');
            $customer->panel()
            ->tools(function ($tools) {
                $tools->disableDelete();
            });
            $customer->name();
        });
        $show->boxs(__('Ô'), function($box) {
            $box->setResource('/admin/boxs');
            $box->panel()
            ->tools(function ($tools) {
                $tools->disableDelete();
            });
            $box->name();
            $box->direction_choose();
        });
        $show->field('title', __('Chức danh'));
        $show->field('address', __('Địa chỉ'));
        $show->field('date_of_birth', __('Ngày sinh'));
        $show->field('date_of_death', __('Ngày mất'));
        $show->field('age', __('Tuổi'));
        $show->duration(__('Thời hạn'))->as(function($duration) {
            return $duration == 0 ? "10 năm (18 TR)" : "50 năm (39 TR)";
        });
        $show->service_charge(__('Phí dịch vụ'))->as(function($service) {
            return $service == 0 ? "200K/1 tháng" : "Miễn phí";
        });
        $show->promotion(__('Khuyến mãi'))->as(function($promo) {
            return $promo . " %";
        });

        $show->money_info(__('Thành tiền'))->as(function($money) {
            return number_format($money) . " VNĐ";
        });

        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->panel()
        ->tools(function ($tools) {
            $tools->disableDelete();
        });
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new HumanDeath);
        $boxService = $this->boxService;
        $humanService = $this->humanService;
        $request = request()->route()->parameters();
        $boxData = [];
        if (count($request) > 0) {
            $data = $form->model()->find($request['human_death']);
            $boxData = $this->boxService->getNameAndIdWithStatus($data->box_id);
        }else {
            $boxData = $this->boxService->getNameAndIdWithStatus($boxId = null);
        }
        $form->column(1/2, function ($form) use ($boxData) {
            $form->display('id', __('ID'));
            $form->text('fullname', __('Họ và tên'))->placeholder('Họ và tên...');
            $form->text('title', __('Chức danh'))->placeholder('Chức danh...');
            $form->select('box_id', __("Ô"))->options($boxData)->rules('required');
            $form->datetime('date_of_birth', __('Ngày sinh'))->format('YYYY-MM-DD');
            $form->datetime('date_of_death', __('Ngày mất'))->format('YYYY-MM-DD');
            $form->number('age', __('Tuổi'))->placeholder('Tuổi...');
            $form->select('customers_id', __("Người liên hệ"))->options($this->customerService->getNameAndId())->rules('required');
            $form->textarea('address', __('Địa chỉ'))->placeholder('Địa chỉ...')->rows(10);
        });

        $form->column(1/2, function ($form) {
           
            $form->select('duration', __('Thời hạn'))->options(["10 năm (18 TR)", "50 năm (39 TR)"])->default(0)->attribute(['id' => 'humanDeathDuration'])->rules('required');
            $form->select('service_charge', __('Phí dịch vụ'))->options(["200K/1 tháng", "Miễn phí"])->default(0)->attribute(['id' => 'humanDeathServiceCharge'])->rules('required');
            $form->currency('promotion', __('Khuyến mãi'))->symbol('%')->default(0)->attribute(['id' => 'humanDeathPromotion']);
            $form->currency('money_info', __('Thành tiền'))->symbol('₫')->default(42000000)->attribute(['id' => 'humanDeathMoneyInfo'])->disable();
            $form->display('created_at', __('Created At'));
            $form->display('updated_at', __('Updated At'));
        });

        $form->saving(function ($form) use ($boxService, $humanService) {
            $durationMonth = $form->duration == 0 ? DURATION_MONTH_DEFAUTL : DURATION_FIFTY_YEARS;
            $priceDuration = $form->duration == 0 ? PRICE_DURATION_DEFAUTL : PRICE_DURATION_FIFTY_YEARS;
            $priceService = $form->service_charge == 0 ? $durationMonth * PRICE_SERVICE_DEFAULT : 0;
            $pricePromo = $form->promotion ? ($priceDuration * $form->promotion) / 100 : 0;
            $form->money_info = $priceDuration + $priceService - $pricePromo;
            if (!$form->model()->id) {
                $data = [
                    "status" => 1
                ];
                $boxService->findOneAndUpdate((int)$form->box_id, $data);
            }else {
                if ($form->box_id) {
                    $oldHumanData = $humanService->getHumanDeathById($form->model()->id);
                    if ($oldHumanData->box_id != $form->box_id) {
                        $boxService->findOneAndUpdate((int)$form->box_id, ["status" => 1]);
                        $boxService->findOneAndUpdate((int)$oldHumanData->box_id, ["status" => 0]);
                    }
                }
            }
        });

        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });
        
        return $form;
    }
}
