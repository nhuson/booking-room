<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Boxs;
use App\Services\BoxTypeService;
use App\Services\CustomerService;
use App\Services\RoomService;
use App\Services\BoxService;

class BoxController extends AdminController
{
    protected $boxtypeService, $customerService, $roomService, $boxService;

    public function __construct(BoxTypeService $boxtypeService, CustomerService $customerService, RoomService $roomService, BoxService $boxService) {
        $this->boxtypeService = $boxtypeService;
        $this->customerService = $customerService;
        $this->roomService = $roomService;
        $this->boxService = $boxService;
    }
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Quản lý ô';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Boxs);
        $grid->column('id', __('ID'))->sortable()->filter();
        $grid->column('name', __('Tên ô'))->limit(30)->editable()->sortable()->filter('like');
        $grid->column('rooms.building_id', __('Toà nhà'))->display(function() {
            return $this->rooms->building->name;
        });
        $grid->column('rooms.name', __('Phòng'));
        $grid->column('boxTypes.name',__('Loại ô'));
        $grid->column('status', __('Trạng thái'))->display(function($status) {
            return $status ? "Hết chỗ" : "Còn trống";
        })->filter();

        $grid->filter(function($filter) {
            $filter->like('name', __('Tên ô'));
            $filter->equal('box_type_id', 'Loại ô')->select($this->boxtypeService->getBoxtype());
            $filter->equal('room_id', 'Phòng')->select($this->roomService->getNameAndId());
            $filter->equal('status', 'Trạng thái')->select(["Còn trống", "Hết chỗ"]);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Boxs::findOrFail($id));
        $show->field('id', __('ID'));
        $show->field('name', __('Tên ô'));
        $show->rooms(__("Phòng"), function($room){
            $room->setResource('/admin/rooms');
            $room->panel()
            ->tools(function ($tools) {
                $tools->disableDelete();
            });
            $room->name();
        });
        $show->boxTypes(__('Loại ô'), function($boxType) {
            $boxType->setResource('/admin/box-type');
            $boxType->panel()
            ->tools(function ($tools) {
                $tools->disableDelete();
            });
            $boxType->name();
        });
        $show->field('status', __('Trạng thái'))->as(function($status) {
            return $status ? "Hết chỗ" : "Còn trống";
        });
        $show->field('direction_choose', __('Chọn hướng'));

        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->panel()
            ->tools(function ($tools) {
                $tools->disableDelete();
            });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Boxs);
        $roomService = $this->roomService;
        $boxService = $this->boxService;
        $form->display('id', __('ID'));
        $form->text('name', __('Tên ô'))->placeholder('Tên ô...')->rules('required');
        $form->select('room_id', 'Phòng')->options($this->roomService->getNameAndId());
        $form->select('box_type_id', __("Loại ô"))->options($this->boxtypeService->getBoxtype());
        $form->select('status', __("Trạng thái"))->options(["Còn trống", "Hết chỗ"]);
        $form->text('direction_choose', __('Chọn hướng'))->placeholder('Chọn hướng...');
        $form->display('created_at', __('Created At'));
        $form->display('updated_at', __('Updated At'));

        $form->saving(function ($form) use ($boxService, $roomService) {
            if (!$form->model()->id) {
                $error = $boxService->handlerForm($form, $boxService, $roomService);
                if ($error) {
                    return back()->with(compact('error'));
                }
            } else {
                if ($form->room_id) {
                    $oldBox = $boxService->getBoxById($form->model()->id);
                    if ((int)$oldBox->room_id != (int)$form->room_id) {
                        $error = $boxService->handlerForm($form, $boxService, $roomService);
                        if ($error) {
                            return back()->with(compact('error'));
                        }
                    }
                }
            }
        });

        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });

        return $form;
    }
}
