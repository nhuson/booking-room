<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Building;
use App\Services\BuildingService;

class BuildingController extends AdminController
{
    protected $buildingService;
    public function __construct(BuildingService $buildingService) {
       $this->buildingService = $buildingService;
    }
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Quản lý toà nhà';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Building);

        $grid->column('id', __('ID'))->sortable()->filter();
        $grid->column('name', __('Tên toà nhà'))->limit(30)->editable()->sortable()->filter('like');
        $grid->column('number_rooms', __('Số lượng phòng'))->editable()->sortable()->filter();
        $grid->column('number_floors', __('Số tầng'))->editable()->sortable()->filter();
        $grid->column('address', __('Địa chỉ'))->limit(30)->editable()->sortable()->filter('like');

        $grid->filter(function($filter) {
            $filter->like('name', __('Tên toà nhà'));
            // $filter->equal('room_id', 'Phòng')->select($this->roomService->getNameAndId());
            $filter->equal('number_rooms', 'Số lượng phòng');
            $filter->equal('number_floors', 'Số tầng');
            $filter->like('address', __('Địa chỉ'));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Building::findOrFail($id));
        $show->field('id', __('ID'));
        $show->field('name', __('Tên toà nhà'));
        $show->field('number_rooms', __('Số lượng phòng'));
        $show->field('number_floors', __('Số tầng'));
        $show->field('address', __('Địa chỉ'))->limit(30);
       
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->panel()
            ->tools(function ($tools) {
                $tools->disableDelete();
            });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Building);
        $buildingService = $this->buildingService;
        $form->display('id', __('ID'));
        $form->text('name', __('Tên toà nhà'))->placeholder('Tên toà nhà...')->rules('required');
        $form->number('number_rooms', __('Số lượng phòng'))->placeholder('Số lượng phòng...')->rules('required');
        $form->number('number_floors', __('Số tầng'))->placeholder('Số tầng...');
        $form->textarea('address', __('Địa chỉ'))->placeholder('Địa chỉ...')->rows(10);

        $form->display('created_at', __('Created At'));
        $form->display('updated_at', __('Updated At'));

        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });

        return $form;
    }
}
