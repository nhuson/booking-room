<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\BoxType;

class BoxtypeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Quản lý loại ô';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BoxType);
        $grid->disableCreateButton();
        $grid->disableRowSelector();
        $grid->disableActions();
        $grid->column('id', __('ID'))->sortable();
        $grid->column('name', __('Tên loại ô'))->editable();
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->filter(function($filter) {
            $filter->like('name', __('Tên loại ô'));
        });
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });
        $grid->actions(function ($actions) {
            $actions->disableDelete();
        });
       
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    // protected function detail($id)
    // {
    //     $show = new Show(BoxType::findOrFail($id));

    //     $show->field('id', __('ID'));
    //     $show->field('name', __('Tên loại ô'));
    //     $show->field('slug', __('Slug'));
    //     $show->field('created_at', __('Created at'));
    //     $show->field('updated_at', __('Updated at'));
    //     $show->panel()
    //     ->tools(function ($tools) {
    //         $tools->disableDelete();
    //     });
    //     return $show;
    // }

    // /**
    //  * Make a form builder.
    //  *
    //  * @return Form
    //  */
    // protected function form()
    // {
    //     $form = new Form(new BoxType);
    //     $form->display('id', __('ID'));
    //     $form->text('name', __('Tên loại ô'))->placeholder('Tên loại ô...')->rules('required');
    //     $form->text('slug', __('Slug'))->placeholder('Slug...');
    //     $form->display('created_at', __('Created At'));
    //     $form->display('updated_at', __('Updated At'));
    //     $form->tools(function (Form\Tools $tools) {
    //         $tools->disableDelete();
    //     });
    //     return $form;
    // }
}
