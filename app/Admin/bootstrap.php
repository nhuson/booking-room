<?php

/**
 * Laravel-admin - admin builder based on Laravel.
 * @author z-song <https://github.com/z-song>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 * Encore\Admin\Form::forget(['map', 'editor']);
 *
 * Or extend custom form field:
 * Encore\Admin\Form::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */
Admin::css(asset('css/Chart.min.css'));
Admin::js(asset('js/admin.js'));
Admin::script('HumanDeathForm.init()');
Admin::js(asset('js/Chart.bundle.min.js'));
Admin::js(asset('js/Chart.min.js'));
Admin::js(asset('js/statistic.js'));
Admin::script('StatisticFinance.init()');
Encore\Admin\Form::forget(['map', 'editor']);
app('view')->prependNamespace('admin', resource_path('views/admin'));
