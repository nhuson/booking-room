<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Interfaces\BoxTypeInterface', 'App\Repositories\BoxTypeRepository');
        $this->app->bind('App\Interfaces\CustomerInterface', 'App\Repositories\CustomerRepository');
        $this->app->bind('App\Interfaces\BoxInterface', 'App\Repositories\BoxRepository');
        $this->app->bind('App\Interfaces\RoomInterface', 'App\Repositories\RoomRepository');
        $this->app->bind('App\Interfaces\BuildingInterface', 'App\Repositories\BuildingRepository');
        $this->app->bind('App\Interfaces\HumanDeathInterface', 'App\Repositories\HumanDeathRepository');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
